# Reduced Representation Libraries Analyses tutorial

Based on [RADCamp Lisbon 2020](https://radcamp.github.io/Lisbon2020/). You can use the resources from there to learn more.


### Important documentation:  
* [ipyrad full documentation](https://ipyrad.readthedocs.io)
* [radcamp home](https://radcamp.github.io/)

## RAD-Seq
### What is RADSeq data?
RADSeq is a technique for creating a reduced representation of genomic
variation of a given set of samples. Restriction endonucleases are used
to randomly fragment genomic DNA, which is followed by several different
possible fragment selection steps.

The core concept of RADSeq is that we want to sequence a random subset of
the total genomic DNA to obtain genome-wide variation at a fraction of the cost
of whole-genome sequencing.

### What does it look like?

The "Original" RAD protocol looks something like this:

![png](A1.5_assets/RAD.png)

> **Figure from Andrews et al 2016.**

### Variants of RAD
Many protocols exist that generate fragments in numerous different ways,
including using 1 or 2 restriction enzymes, with or without a PCR step,
including multiplexed barcodes to further increase sample throughput, and so on.
It's also possible to sequence a library as single-end or paired-end, adding
further information, but also somewhat further complications in assembly. Here
are a couple more conceptual diagrams for different protocols (again from
Andrews).

![png](A1.5_assets/GBS.png)
![png](A1.5_assets/ddRAD.png)

## Getting started

First thing, make sure you have `miniconda` installed in your VM. Once you have it up and running you can use it to install `ipyrad`:

``` bash
# Add conda-forge channel
conda config --add channels conda-forge

# Make sure conda is up to date
conda update conda
# Try mamba (optional step)
conda install mamba

# Create and activate a new conda environment
conda create -n ipyrad
conda activate ipyrad

# Install ipyrad
conda install ipyrad nympy=1.19 -c bioconda  # Due to a numpy update on 27/05/2023 you have to use this workaround.
# This step can take quite a while to solve. It is recommended that you use "mamba" for this.
mamba install ipyrad numpy=1.19 -c bioconda
```

## Fetch and unpack the simulated data
First we will practice good data management habits by creating a new directory
for the workshop materials, switching to this directory, and fetching and
unpacking the simulated data

``` bash
# Practice good data management habits. Make a directory for the tutorial.
(ipyrad)$ cd ~
(ipyrad)$ mkdir ipyrad-assembly
(ipyrad)$ cd ipyrad-assembly

# Fetch ipyrad github repo (for the simulated data)
(ipyrad)$ git clone https://github.com/dereneaton/ipyrad --depth=1

# Extract simulated data
(ipyrad)$ tar -xvzf ipyrad/tests/ipsimdata.tar.gz
```


## Overview of Assembly Steps
Very roughly speaking, ipyrad exists to transform raw data coming off the 
sequencing instrument into output files that you can use for downstream 
analysis. 

![png](A1.5_assets/02_ipyrad_partI_CLI_ipyrad_workflow.png)

The basic steps of this process are as follows:

* Step 1 - Demultiplex/Load Raw Data
* Step 2 - Trim and Quality Control
* Step 3 - Cluster or reference-map within Samples
* Step 4 - Calculate Error Rate and Heterozygosity
* Step 5 - Call consensus sequences/alleles
* Step 6 - Cluster across Samples
* Step 7 - Apply filters and write output formats

> **Note on files in the project directory:** Assembling RAD-seq type 
sequence data requires a lot of different steps, and these steps 
generate a **lot** of intermediary files. ipyrad organizes these files 
into directories, and it prepends the name of your assembly to each 
directory with data that belongs to it. One result of this is that 
you can have multiple assemblies of the same raw data with different 
parameter settings and you don't have to manage all the files yourself! 
(See [Branching assemblies](https://ipyrad.readthedocs.io/en/latest/8-branching.html)
for more info). Another result is that **you should not rename or move any of
the directories inside your project directory**, unless you know what you're
doing or you don't mind if your assembly breaks.


## ipyrad help
To better understand how to use ipyrad, let's take a look at the help argument.
We will use some of the ipyrad arguments in this tutorial (for example: -n, -p,
-s, -c, -r). The complete list of optional arguments and their explanation can
be accessed with the `--help` flag:

``` bash
$ ipyrad --help
usage: ipyrad [-h] [-v] [-r] [-f] [-q] [-d] [-n NEW] [-p PARAMS] [-s STEPS]
              [-b [BRANCH [BRANCH ...]]] [-m [MERGE [MERGE ...]]] [-c cores]
              [-t threading] [--MPI] [--ipcluster [IPCLUSTER]]
              [--download [DOWNLOAD [DOWNLOAD ...]]]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -r, --results         show results summary for Assembly in params.txt and
                        exit
  -f, --force           force overwrite of existing data
  -q, --quiet           do not print to stderror or stdout.
  -d, --debug           print lots more info to ipyrad_log.txt.
  -n NEW                create new file 'params-{new}.txt' in current
                        directory
  -p PARAMS             path to params file for Assembly:
                        params-{assembly_name}.txt
  -s STEPS              Set of assembly steps to run, e.g., -s 123
  -b [BRANCH [BRANCH ...]]
                        create new branch of Assembly as params-{branch}.txt,
                        and can be used to drop samples from Assembly.
  -m [MERGE [MERGE ...]]
                        merge multiple Assemblies into one joint Assembly, and
                        can be used to merge Samples into one Sample.
  -c cores              number of CPU cores to use (Default=0=All)
  -t threading          tune threading of multi-threaded binaries (Default=2)
  --MPI                 connect to parallel CPUs across multiple nodes
  --ipcluster [IPCLUSTER]
                        connect to running ipcluster, enter profile name or
                        profile='default'
  --download [DOWNLOAD [DOWNLOAD ...]]
                        download fastq files by accession (e.g., SRP or SRR)

  * Example command-line usage: 
    ipyrad -n data                       ## create new file called params-data.txt 
    ipyrad -p params-data.txt -s 123     ## run only steps 1-3 of assembly.
    ipyrad -p params-data.txt -s 3 -f    ## run step 3, overwrite existing data.

  * HPC parallelization across 32 cores
    ipyrad -p params-data.txt -s 3 -c 32 --MPI

  * Print results summary 
    ipyrad -p params-data.txt -r 

  * Branch/Merging Assemblies
    ipyrad -p params-data.txt -b newdata  
    ipyrad -m newdata params-1.txt params-2.txt [params-3.txt, ...]


  * Subsample taxa during branching
    ipyrad -p params-data.txt -b newdata taxaKeepList.txt

  * Download sequence data from SRA into directory 'sra-fastqs/' 
    ipyrad --download SRP021469 sra-fastqs/

  * Documentation: http://ipyrad.readthedocs.io
```


## Create a new parameters file
ipyrad uses a text file to hold all the parameters for a given assembly.
Start by creating a new parameters file with the `-n` flag. This flag
requires you to pass in a name for your assembly. In the example we use
`simdata` but the name can be anything at all. Once you start
analysing your own data you might call your parameters file something
more informative, like the name of your organism and some details on the
settings.

``` bash 
# Make sure you're in your ipyrad-assembly directory
$ pwd

# create a new params file named 'simdata'
$ ipyrad -n simdata
```

This will create a file in the current directory called `params-simdata.txt`. The 
params file lists on each line one parameter followed by a \#\# mark, then the name of the 
parameter, and then a short description of its purpose. Lets take a look at it.

``` bash
$ cat params-simdata.txt
------- ipyrad params file (v.0.9.26)-------------------------------------------
simdata                        ## [0] [assembly_name]: Assembly name. Used to name output directories for assembly steps
~/ipyrad-assembly              ## [1] [project_dir]: Project dir (made in curdir if not present)
                               ## [2] [raw_fastq_path]: Location of raw non-demultiplexed fastq files
                               ## [3] [barcodes_path]: Location of barcodes file
                               ## [4] [sorted_fastq_path]: Location of demultiplexed/sorted fastq files
denovo                         ## [5] [assembly_method]: Assembly method (denovo, reference)
                               ## [6] [reference_sequence]: Location of reference sequence file
rad                            ## [7] [datatype]: Datatype (see docs): rad, gbs, ddrad, etc.
TGCAG,                         ## [8] [restriction_overhang]: Restriction overhang (cut1,) or (cut1, cut2)
5                              ## [9] [max_low_qual_bases]: Max low quality base calls (Q<20) in a read
33                             ## [10] [phred_Qscore_offset]: phred Q score offset (33 is default and very standard)
6                              ## [11] [mindepth_statistical]: Min depth for statistical base calling
6                              ## [12] [mindepth_majrule]: Min depth for majority-rule base calling
10000                          ## [13] [maxdepth]: Max cluster depth within samples
0.85                           ## [14] [clust_threshold]: Clustering threshold for de novo assembly
0                              ## [15] [max_barcode_mismatch]: Max number of allowable mismatches in barcodes
0                              ## [16] [filter_adapters]: Filter for adapters/primers (1 or 2=stricter)
35                             ## [17] [filter_min_trim_len]: Min length of reads after adapter trim
2                              ## [18] [max_alleles_consens]: Max alleles per site in consensus sequences
0.05                           ## [19] [max_Ns_consens]: Max N's (uncalled bases) in consensus
0.05                           ## [20] [max_Hs_consens]: Max Hs (heterozygotes) in consensus
4                              ## [21] [min_samples_locus]: Min # samples per locus for output
0.2                            ## [22] [max_SNPs_locus]: Max # SNPs per locus
8                              ## [23] [max_Indels_locus]: Max # of indels per locus
0.5                            ## [24] [max_shared_Hs_locus]: Max # heterozygous sites per locus
0, 0, 0, 0                     ## [25] [trim_reads]: Trim raw read edges (R1>, <R1, R2>, <R2) (see docs)
0, 0, 0, 0                     ## [26] [trim_loci]: Trim locus edges (see docs) (R1>, <R1, R2>, <R2)
p, s, l                        ## [27] [output_formats]: Output formats (see docs)
                               ## [28] [pop_assign_file]: Path to population assignment file
                               ## [29] [reference_as_filter]: Reads mapped to this reference are removed in step 3
```

In general the defaults are sensible, and we won't mess with them for now, 
but there are a few parameters we *must* change: the path to the raw data, 
the dataype, the restriction overhang sequence, and the barcodes file.

Use your favourite text editor to modify `params-simdata.txt` and change
these parameters:

``` bash
# Using nano
$ nano params-simdata.txt

# Alternatively use Vim
$ vim params-simdata.txt
```

We need to specify where the raw data files are located, the type of data we
are using (.e.g., 'gbs', 'rad', 'ddrad', 'pairddrad), and which enzyme cut site
overhangs are expected to be present on the reads. Below are the parameter
setings you'll need to change for the simulated single-end RAD example data:

``` bash
./ipsimdata/rad_example_R1_.fastq.gz        ## [2] [raw_fastq_path]: Location of raw non-demultiplexed fastq files
./ipsimdata/rad_example_barcodes.txt        ## [3] [barcodes_path]: Location of barcodes file
rad                            ## [7] [datatype]: Datatype (see docs): rad, gbs, ddrad, etc.
TGCAG,                         ## [8] [restriction_overhang]: Restriction overhang (cut1,) or (cut1, cut2)
```

Once we start running the analysis ipyrad will create several new 
directories to hold the output of each step for this assembly. By 
default the new directories are created in the `project_dir`
directory and use the prefix specified by the `assembly_name` parameter.
Because we use the default (`~/ipyrad-assembly`) for the `project_dir` for
this tutorial, all these intermediate directories will be of the form:
`~/ipyrad-assembly/simdata_*`, or the analagous name that you used for your
assembly name.


## Step 1: Demultiplexing the raw data

Commonly, sequencing facilities will give you one giant .gz file that contains
all the reads from all the samples all mixed up together. Step 1 is all about
sorting out which reads belong to which samples, so this is where the barcodes
file comes in handy. The barcodes file is a simple text file mapping sample
names to barcode sequences. Lets look at the simulated barcodes:

``` bash
$ cat ./ipsimdata/rad_example_barcodes.txt
1A_0    CATCATCAT
1B_0    CCAGTGATA
1C_0    TGGCCTAGT
1D_0    GGGAAAAAC
2E_0    GTGGATATC
2F_0    AGAGCCGAG
2G_0    CTCCAATCC
2H_0    CTCACTGCA
3I_0    GGCGCATAC
3J_0    CCTTATGTC
3K_0    ACGTGTGTG
3L_0    TTACTAACA
```

Here the barcodes are all the same length, but ipyrad can also handle variable
length barcodes, and in some cases multiplexed barcodes (3RAD and variants). We
can also allow for varying amounts of sequencing error in the barcode sequences
(parameter 15, `max_barcode_mismatch`).

> **Note on step 1:** Occasionally sequencing facilities will send back data
already demultiplexed to samples. This is totally fine, and is handled natively
by ipyrad. In this case you would use the `sorted_fastq_path` in the params file
to indiciate the sample fastq.gz files. ipyrad will then scan the samples and
load in the raw data.

Now lets run step 1! For the simulated data this will take <10 seconds.

> **Special Note:** In command line mode please be aware to *always* specify
the number of cores with the `-c` flag. If you do not specify the number of 
cores ipyrad assumes you want **all** of them, which will result in you
hogging up all the CPU on whatever computer you're on. This is fine inside the
VM, but if you're on an HPC this can cause problems!

``` bash
## -p    the params file we wish to use
## -s    the step to run
## -c    the number of cores to allocate   <-- Important!
$ ipyrad -p params-simdata.txt -s 1 -c 3

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 1: Demultiplexing fastq data to Samples
  [####################] 100% 0:00:10 | sorting reads          
  [####################] 100% 0:00:02 | writing/compressing    

  Parallel connection closed.
```

## Looking at intermediate results (optional)

The results of step 1 are saved in the `*_fastqs` directory, where `*` is a
wildcard that stands in for the name of your assembly. In our case it is
`simdata_fastqs`. Lets look inside this directory:

``` bash
$ ls simdata_fastqs/
1A_0_R1_.fastq.gz  1C_0_R1_.fastq.gz  2E_0_R1_.fastq.gz  2G_0_R1_.fastq.gz  3I_0_R1_.fastq.gz  3K_0_R1_.fastq.gz  s1_demultiplex_stats.txt
1B_0_R1_.fastq.gz  1D_0_R1_.fastq.gz  2F_0_R1_.fastq.gz  2H_0_R1_.fastq.gz  3J_0_R1_.fastq.gz  3L_0_R1_.fastq.gz
```

As a convenience, ipyrad internally tracks the state of all your steps in your 
current assembly, so at any time you can ask for results by invoking the `-r` flag.
We also use the `-p` flag to tell it which params file (i.e., which assembly) we 
want to print stats for.

``` bash
## -r fetches informative results from currently executed steps  
$ ipyrad -p params-simdata.txt -r
```

```
Summary stats of Assembly simdata
------------------------------------------------
      state  reads_raw
1A_0      1      19862
1B_0      1      20043
1C_0      1      20136
1D_0      1      19966
2E_0      1      20017
2F_0      1      19933
2G_0      1      20030
2H_0      1      20199
3I_0      1      19885
3J_0      1      19822
3K_0      1      19965
3L_0      1      20008

Full stats files
------------------------------------------------
step 1: ~/ipyrad-analysis/simdata_fastqs/s1_demultiplex_stats.txt
step 2: None
step 3: None
step 4: None
step 5: None
step 6: None
step 7: None
```

If you want to get even **more** info ipyrad tracks all kinds of wacky
stats and saves them to files inside the directories it creates for
each step. For instance to see full stats for step 1 (the wackyness
of the step 1 stats at this point isn't very interesting, but we'll
see stats for later steps are more verbose):

``` bash 
$  cat simdata_fastqs/s1_demultiplex_stats.txt
```

```
raw_file                               total_reads    cut_found  bar_matched
rad_example_R1_.fastq                       239866       239866       239866

sample_name                            total_reads
1A_0                                         19862
1B_0                                         20043
1C_0                                         20136
1D_0                                         19966
2E_0                                         20017
2F_0                                         19933
2G_0                                         20030
2H_0                                         20199
3I_0                                         19885
3J_0                                         19822
3K_0                                         19965
3L_0                                         20008

sample_name                               true_bar       obs_bar     N_records
1A_0                                     CATCATCAT     CATCATCAT         19862
1B_0                                     CCAGTGATA     CCAGTGATA         20043
1C_0                                     TGGCCTAGT     TGGCCTAGT         20136
1D_0                                     GGGAAAAAC     GGGAAAAAC         19966
2E_0                                     GTGGATATC     GTGGATATC         20017
2F_0                                     AGAGCCGAG     AGAGCCGAG         19933
2G_0                                     CTCCAATCC     CTCCAATCC         20030
2H_0                                     CTCACTGCA     CTCACTGCA         20199
3I_0                                     GGCGCATAC     GGCGCATAC         19885
3J_0                                     CCTTATGTC     CCTTATGTC         19822
3K_0                                     ACGTGTGTG     ACGTGTGTG         19965
3L_0                                     TTACTAACA     TTACTAACA         20008
no_match                                         _            _            0
```

Another early indicator of trouble is if you have a **ton** of reads that are
`no_match`. This means maybe your barcodes file is wrong, or maybe your library
prep went poorly. Here, with the simulated data we have no unmatched barcodes,
because, well, it's simulated.


# Step 2: Filter reads

This step filters reads based on quality scores and maximum number of
uncalled bases, and can be used to detect Illumina adapters in your 
reads, which is sometimes a problem under a couple different library 
prep scenarios. Since it's not atypical to have adapter contamination
issues and to have a little noise toward the distal end of the reads
lets imagine this is true of the simulated data, and we'll try to account
for this by trimming reads to 90bp and using aggressive adapter filtering. 

Edit your params file again with your favourite text editor:

``` bash
nano params-simdata.txt
# Alternative
vim params-simdata.txt
```

and change the following two parameter settings:

```
2                               ## [16] [filter_adapters]: Filter for adapters/primers (1 or 2=stricter)
0, 90, 0, 0                     ## [25] [trim_reads]: Trim raw read edges (R1>, <R1, R2>, <R2) (see docs)
```

``` bash
$ ipyrad -p params-simdata.txt -s 2 -c 3
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 2: Filtering and trimming reads
  [####################] 100% 0:00:08 | processing reads     

  Parallel connection closed.
```

The filtered files are written to a new directory called `simdata_edits`. Again,
you can look at the results output by this step and also some handy stats
tracked for this assembly.


``` bash
## Get current stats including # raw reads and # reads after filtering.
$ ipyrad -p params-simdata.txt -r
```


# Step 3: clustering within-samples

Step 3 de-replicates and then clusters reads within each sample by the
set clustering threshold and then writes the clusters to new files in a
directory called `simdata_clust_0.85`. Intuitively we are trying to
identify all the reads that map to the same locus within each sample.
The clustering threshold specifies the minimum percentage of sequence
similarity below which we will consider two reads to have come from
different loci.

The true name of this output directory will be dictated by the value you
set for the `clust_threshold` parameter in the params file. This makes it
very easy to test different clustering thresholds, and keep the different
runs organized (since you will have for example `simdata_clust_0.85` and
`simdata_clust_0.9`).

You can see the default value is 0.85, so our default directory is named
accordingly. This value dictates the percentage of sequence similarity
that reads must have in order to be considered reads at the same locus.
You'll more than likely want to experiment with this value, but 0.85 is
a reliable default, balancing over-splitting of loci vs over-lumping.
Don't mess with this until you feel comfortable with the overall
workflow, and also until you've learned about
[Branching assemblies](https://ipyrad.readthedocs.io/en/latest/8-branching.html).

There have been many papers written comparing how results of assemblies vary 
depending on the clustering threshold. In general, my advice is to use a value
between about .82 and .95. Within this region results typically do not vary too
significantly, whereas above .95 you will oversplit loci and recover fewer SNPs.

It's also possible to incorporate information from a reference
genome to improve clustering at this step, if such a resources is
available for your organism (or one that is relatively closely related).
Youc can refer to the [ipyrad documentation](https://ipyrad.readthedocs.io/en/latest/tutorial_advanced_cli.html#reference-sequence-mapping)
for more information on this subject.

> **Note on performance:** Steps 3 and 6 generally take considerably 
longer than any of the other steps, due to the resource intensive clustering 
and alignment phases. These can take on the order of 10-100x as long 
as the next longest running step. This depends heavily on the number of samples
in your dataset, the number of cores on your computer, the length(s) of your
reads, and the "messiness" of your data in terms of the number of unique loci
present (this can vary from a few thousand to many millions).

Now lets run step 3:

``` bash
$ ipyrad -p params-simdata.txt -s 3 -c 3
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 3: Clustering/Mapping reads within samples
  [####################] 100% 0:00:02 | dereplicating          
  [####################] 100% 0:00:03 | clustering/mapping     
  [####################] 100% 0:00:00 | building clusters      
  [####################] 100% 0:00:00 | chunking clusters      
  [####################] 100% 0:00:48 | aligning clusters      
  [####################] 100% 0:00:00 | concat clusters        
  [####################] 100% 0:00:00 | calc cluster stats     

  Parallel connection closed.
```

### Looking at intermediate files

You now know how to examine these intermediate files. Doing so is out of scope for this course, but feel free to do it if you feel so inclined.


# Step 4: Joint estimation of heterozygosity and error rate

After this step we will be a little shorter on details. For this course you are not required to know how intermediate steps work, but, as always, feel free to do some research on your own.

``` bash
$ ipyrad -p params-simdata.txt -s 4 -c 3
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 4: Joint estimation of error rate and heterozygosity
  [####################] 100% 0:00:12 | inferring [H, E]       

  Parallel connection closed.
```


# Step 5: Consensus base calls

``` bash
$ ipyrad -p params-simdata.txt -s 5 -c 3
```

```
loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 5: Consensus base/allele calling 
  Mean error  [0.00076 sd=0.00001]
  Mean hetero [0.00192 sd=0.00011]
  [####################] 100% 0:00:02 | calculating depths     
  [####################] 100% 0:00:00 | chunking clusters      
  [####################] 100% 0:00:47 | consens calling        
  [####################] 100% 0:00:01 | indexing alleles       

  Parallel connection closed.
```

In-depth operations of step 5:
* calculating depths - A simple refinement of the H/E estimates.
* chunking clusters - Again, breaking big files into smaller chunks to aid
parallelization.
* consensus calling - Actually perform the consensus sequence calling
* indexing alleles - Keeping track of phase information


# Step 6: Cluster across samples

Step 6 clusters consensus sequences across samples.

``` bash
$ ipyrad -p params-simdata.txt -s 6 -c 3
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 6: Clustering/Mapping across samples 
  [####################] 100% 0:00:02 | concatenating inputs   
  [####################] 100% 0:00:03 | clustering across    
  [####################] 100% 0:00:02 | building clusters      
  [####################] 100% 0:00:13 | aligning clusters      

  Parallel connection closed.
```

In-depth operations of step 6:
* concatenating inputs - Gathering all consensus files and preprocessing to
improve performance
* clustering across - Cluster by similarity threshold across samples
* building clusters - Group similar reads into clusters
* aligning clusters - Align within each cluster


# Step 7: Filter and write output files

The final step is to filter the data and write output files in many convenient
file formats.

``` bash
$ ipyrad -p params-simdata.txt -s 7 -c 3
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 7: Filtering and formatting output files 
  [####################] 100% 0:00:12 | applying filters       
  [####################] 100% 0:00:02 | building arrays        
  [####################] 100% 0:00:00 | writing conversions    

  Parallel connection closed.
```

A new directory is created called `simdata_outfiles`, and you may inspect
the contents:

``` bash
$ ls simdata_outfiles/
```

```
simdata.loci  simdata.phy  simdata.seqs.hdf5  simdata.snps  simdata.snps.hdf5  simdata.snpsmap  simdata_stats.txt
```

This directory contains all the output files specified by the `output_formats`
parameter in the params file. The defaults will create two different version of
phylip output, one including the full sequence `simdata.phy` and one including
only variable sites `simdata.snps`, as well as a `.snpsmap` file which
indicates the location of each snp within each locus, a `simdata.vcf` file
including all the SNPs in VCF format, and the `simdata.loci` file which is
ipyrad's internal format recording the full sequence for each sample at each
locus. The full list of available output formats and detailed explanations of
each of these is available in the [ipyrad output formats documentation](https://ipyrad.readthedocs.io/en/latest/output_formats.html#full-output-formats).

> **NB:** To get generate all available output files from ipyrad step 7 you can
specify `*` for the `output_formats` parameter in the params file.

The other important file generated by step 7 is the `simdata_stats.txt` which
gives extensive and detailed stats about the final assembly. A quick overview
of the blocks in the stats file:

## Rerunning step 7 to include all output formats

For our downstream analysis we'll need more than just the default output
formats, so lets rerun step 7 and generate all supported output formats. This
can be accomplished by editing the `params-simdata.txt` and setting the
requested `output_formats` to `*` (again, the wildcard character):

```
*                        ## [27] [output_formats]: Output formats (see docs)
```

After this we must now re-run step 7, but this time including the `-f`
flag, to force overwriting the output files that were previously generated. 

``` bash
$ ipyrad -p params-simdata.txt -s 7 -c 3 -f
```

```
  loading Assembly: simdata
  from saved path: ~/ipyrad-assembly/simdata.json

 -------------------------------------------------------------
  ipyrad [v.0.9.26]
  Interactive assembly and analysis of RAD-seq data
 ------------------------------------------------------------- 
  Parallel connection | radcamp2020-VirtualBox: 3 cores
  
  Step 7: Filtering and formatting output files 
  [####################] 100% 0:00:12 | applying filters       
  [####################] 100% 0:00:03 | building arrays        
  [####################] 100% 0:00:04 | writing conversions    
  [####################] 100% 0:00:01 | indexing vcf depths    
  [####################] 100% 0:00:01 | writing vcf output     

  Parallel connection closed.
```

Congratulations! You've completed your first RAD-Seq assembly.

Now that you are done, you should also know you can run all steps at once:

``` bash
$ ipyrad -p params-simdata.txt -s 1234567 -c 3
```

Try to open the `.phy` file with `Aliview`, and use a text editor of your choice to look at the `.vcf` file.

What kind of *biological* analyses do you think these file types are useful for?
