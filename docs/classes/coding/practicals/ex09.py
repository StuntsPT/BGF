#!/usr/bin/env python3

import sys

class GeneLocation():
    """
    A class to store information about genes and their positions in the
    chromossome.
    Has methods to calculate a gene's distance from the chromosome's
    centromere and from other genes.
    """
    def __init__(self, chr_name, position, name):
        self.position = position
        self.chr_name = chr_name
        self.name = name

    def distance_from_origin(self, centromere_position):
        dist = self.position - centromere_position
        if dist < 0:
            prefix = "p"
        else:
            prefix = "q"
        return f"The gene {self.name} is {abs(dist)}{prefix} bp away from the centromere."

    def distance_from_gene(self, other_gene):
        distance = abs(self.position - other_gene.position)
        return f"The genes {self.name} and {other_gene.name} are {distance} bp apart."

    def get_full_coordinates(self):
        return(f"{self.chr_name}:{self.position}")


DHH = GeneLocation("Chr12", 49086656, "DHH")
print(DHH.get_full_coordinates())
print(DHH.distance_from_origin(35500000))

KERA = GeneLocation("Chr12", 91050491, "KERA")
print(KERA.get_full_coordinates())
print(KERA.distance_from_origin(35500000))
print(KERA.distance_from_gene(DHH))

PLBD1 = GeneLocation("Chr12", 14503661, "PLBD1")
print(PLBD1.get_full_coordinates())
print(PLBD1.distance_from_origin(35500000))
print(PLBD1.distance_from_gene(DHH))

TESPA1 = GeneLocation("Chr12", 54948015, "TESPA1")
print(TESPA1.get_full_coordinates())
print(TESPA1.distance_from_origin(35500000))
print(TESPA1.distance_from_gene(DHH))


# Challenge

my_genes = ["DHH", "KERA", "PLBD1", "TESPA1"]
my_positions = {}
with open(sys.argv[1], "r") as gff_file:
    for lines in gff_file:
        for gene in my_genes:
            if(lines.find(f"gene={gene}")!=-1):
                if lines.split()[2] == "mRNA":
                    my_positions[gene] = int(lines.split()[3])
                    my_genes.remove(gene)
                    break

my_objects = []
for k, v in my_positions.items():
    my_objects.append(GeneLocation("Chr12", v, k))

for obj in my_objects:
    print(obj.get_full_coordinates())
    print(obj.distance_from_origin(35500000))
    print(obj.distance_from_gene(my_objects[0]))
