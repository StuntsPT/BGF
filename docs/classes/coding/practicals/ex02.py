#!/usr/bin/env python3

# Ex01
plant_sp_1 = "Limonium vulgare"
plant_sp_2 = "Quercus suber"
plant_sp_3 = "Arabidopsis thaliana"

print(plant_sp_1, plant_sp_2, plant_sp_3, sep=" | ")


# Ex02
plant_sp_1_txid = 70358
plant_sp_2_txid = 58331
plant_sp_3_txid = 3702

print(type(plant_sp_1_txid))
print(type(plant_sp_2_txid))
print(type(plant_sp_3_txid))

print("The species is: " + plant_sp_1 + ". It's txid is " + str(plant_sp_1_txid) + ".")  # One day I will teach you about 'f' strings!

print(int("2"))  # Works
print(int("Hello"))  # Throws an error


# Ex03
animal_species = ["Canis lupus", "Caretta caretta", "Ochotona daurica"]
animal_species_txids = (9612, 8467, 130827)

plant_species = [plant_sp_1, plant_sp_2, plant_sp_3]
plant_txids = (plant_sp_1_txid, plant_sp_2_txid, plant_sp_3_txid)

print(animal_species)
print(animal_species_txids)
print(plant_species)
print(plant_txids)

print(plant_species[1])

companion_data = [(animal_species[0], animal_species_txids[0]),
                  (animal_species[1], animal_species_txids[1]),
                  (animal_species[2], animal_species_txids[2])]
print(companion_data)
print(companion_data[2])


# Ex04
species_per_kingdom = {"animal": animal_species,
                       "plant": plant_species,
                       "fungi": ["Saccharomyces cerevisiae", "Amanita muscaria", "Hemileia vastatrix"]}

print(species_per_kingdom)
print(species_per_kingdom["animal"])
print(species_per_kingdom["plant"])
print(species_per_kingdom["fungi"])


names_and_ids = [(animal_species[0]: animal_species_txids[0]),
                 (animal_species[1]: animal_species_txids[1]),
                 (animal_species[2]: animal_species_txids[2])]
print(names_and_ids)
print(names_and_ids["Canis lupus"])


# Ex05
my_gene_list = ["Cytochrome b",
                "Cytochrome b",
                "Cytochrome b",
                "D-loop",
                "D-loop"]
print(my_gene_list)

my_gene_set = set(my_gene_list)
print(list(my_gene_set))
